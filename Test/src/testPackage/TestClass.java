package testPackage;

import java.net.*;
import java.io.*;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class TestClass {
    static String currencies[] = {"BTC","EUR","USD","GBP"};
    static String accounts[] = {"c5c8737f-6e13-4b5a-9ef3-285f49bc05b7", "95b22bb7-1bee-4bc5-9555-52689137eb49", "d96d23be-30c9-4243-a9ab-e432e9a6f71d", "47d56f1f-9db1-4421-8ba9-804558d2106e"};
    static double amounts[] = {0.1, 10, 50, 100};

    static double s_persent = 0.01;

    public static void main(String[] args) throws Exception {
        mainWorkTest();
    }

    public static HttpsURLConnection sendRequest(double amount, String currency, String from_account, String to_account) throws Exception {
        String url = "https://sandbox.cryptopay.me/api/v2/exchange_transfers";
        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

        //add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        con.setRequestProperty("X-Api-Key", "acea9d6d570540bb7d0e0f077182ffdc");
        con.setRequestProperty("charset", "utf-8");

        String urlParameters = "amount="+amount+"&amount_currency="+currency+"&from_account="+from_account+"&to_account="+to_account;

        //send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + urlParameters);
        return con;
    }

    public static Map<String, String> createResponse(HttpsURLConnection con) throws Exception {
        BufferedReader in = getResponseByConnection(con);
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        System.out.println(response.toString());

        String value = response.toString();
        value = value.substring(1, value.length()-1);
        String[] keyValuePairs = value.split(",");
        Map<String,String> map = new HashMap<>();

        for(String pair : keyValuePairs)
        {
            String[] entry = pair.split(":");
            entry[0].trim();
            entry[1].trim();
            entry[0] = entry[0].substring(1, entry[0].length()-1);
            if(entry[1].startsWith("\""))
                entry[1] = entry[1].substring(1, entry[1].length()-1);
            map.put(entry[0], entry[1]);
        }
        return map;
    }

    public static int getResponseCodeByConnection (HttpsURLConnection con)  throws Exception {
        int responseCode = con.getResponseCode();
        System.out.println("Response Code : " + responseCode);
        return responseCode;
    }

    public static BufferedReader getResponseByConnection (HttpsURLConnection con)  throws Exception {
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        return in;
    }

    public static String getParamByName(String name, Map<String, String> resp) throws Exception {
        return resp.get(name);
    }

    //base transfer test
    public static void mainWorkTest() throws Exception {
        int j = 0;
        for (int i = 0; i < currencies.length; i++) {
            j = i+1;
            if (j == currencies.length)
                j = 0;
            System.out.println("------------------------");
            System.out.println("Exchange transfer CURRENCIES: " +currencies[i]+" ==> " + currencies[j]);
            System.out.println("Exchange transfer ACCOUNTS: " +accounts[i]+" ==> " + accounts[j]);
            HttpsURLConnection con = sendRequest(amounts[i], currencies[i], accounts[i], accounts[j]);
            int code = getResponseCodeByConnection(con)/100;
            switch (code){
                case 2:
                    System.out.println("SUCCESS");
                    Map<String, String> response = createResponse(con);
                    checkCommission(amounts[i], currencies[i], response);
                    checkTransaction(amounts[i], accounts[i], accounts[j], response);
                    break;
                case 4:
                    System.out.println("Client ERROR");
                    break;
                case 5:
                    System.out.println("Server ERROR");
                    break;

            }
        }
    }

    //comission test
    public static void checkCommission(double amount, String amount_currency, Map<String, String> resp) throws Exception {
        double fee = Double.parseDouble(getParamByName("fee", resp));
        String fee_currency = getParamByName("fee_currency", resp);

        if (fee != amount * s_persent) {
            System.out.println("ERROR: Invalid fee value");
            System.out.println("Calculate fee value: " + amount * s_persent + " From server fee value: " + fee);
        } else
            System.out.println("Fee - OK");

        if (!amount_currency.equals(fee_currency)) {
            System.out.println("ERROR: Invalid fee currency");
            System.out.println("Amount currency: " + amount_currency + " Fee currency: " + fee_currency);
        } else
            System.out.println("Fee currency - OK");
    }

    //amount test
    public static void checkTransaction(double amount, String from_account, String to_account, Map<String, String> resp) throws Exception {
        double credit_amount = Double.parseDouble(getParamByName("credit_amount", resp));
        double rate = Double.parseDouble(getParamByName("rate", resp));
        String from_account_server = getParamByName("from_account", resp);
        String to_account_server = getParamByName("to_account", resp);
        double fee = amount * s_persent;
        String str = String.format(Locale.US, "%1.2f", (amount - fee) * rate);
        if (getParamByName("from_currency", resp).equals("BTC")) {
            str = String.format(Locale.US, "%1.4f", (amount - fee) / rate);
        }
        double calc_amount = Double.valueOf(str);

        if (credit_amount == calc_amount) {
            System.out.println("Credit amount - OK");
        } else if (credit_amount - Math.pow(10, -3) <= calc_amount && calc_amount <= credit_amount + Math.pow(10, -3)) {
            System.out.println("WARNING: Small deviation of the credit amount");
            System.out.println("Calculate credit amount: " + calc_amount + " From server credit amount: " + credit_amount);
        } else {
            System.out.println("ERROR: Invalid credit amount value");
            System.out.println("Calculate credit amount: " + calc_amount + " From server credit amount: " + credit_amount);
        }

        if (!from_account.equals(from_account_server)) {
            System.out.println("ERROR: From accounts don't match");
            System.out.println("From account : " + from_account + " From server from account: " + from_account_server);
        } else
            System.out.println("From accounts correct - OK");

        if (!to_account.equals(to_account_server)) {
            System.out.println("ERROR: To accounts don't match");
            System.out.println("To account : " + to_account + " From server to account: " + to_account_server);
        } else
            System.out.println("To accounts correct - OK");
    }
}
